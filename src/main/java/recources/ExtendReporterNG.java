package recources;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ExtendReporterNG {
	static ExtentReports extent;

	public static ExtentReports getReportObject() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-HH-mm");
		LocalDateTime now = LocalDateTime.now();
		String time = dtf.format(now);
		String path = "../automation-test-project/reports/index"+time+".html";
		ExtentSparkReporter reporter = new ExtentSparkReporter(path);
		reporter.config().setReportName("Web Automation Results");
		reporter.config().setDocumentTitle("Test Results");
		extent = new ExtentReports();
		extent.attachReporter(reporter);
		extent.setSystemInfo("Tester", "EXAMPLE TESTER AMINE");
		return extent;
	}
}
