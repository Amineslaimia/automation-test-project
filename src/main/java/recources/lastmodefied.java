package recources;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

public class lastmodefied {

public static File getLastReport() {
    String filePath = "../automation-test-project/reports/";
   return findUsingIOApi(filePath);
}
    public static File findUsingIOApi(String sdir) {
        File dir = new File(sdir);
        if (dir.isDirectory()) {
            Optional<File> opFile = Arrays.stream(dir.listFiles(File::isFile))
                    .max((f1, f2) -> Long.compare(f1.lastModified(), f2.lastModified()));

            if (opFile.isPresent()){
                return opFile.get();
            }
        }

        return null;
    }
}