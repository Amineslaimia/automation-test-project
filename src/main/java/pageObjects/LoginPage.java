package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	public WebDriver driver;
	By email = By.cssSelector("input[placeholder='Email']");
	By password = By.cssSelector("input[placeholder='password']");
	By login = By.cssSelector("button[class='coa-button']");
	By code = By.cssSelector("input[placeholder='code']");
	By submit = By.cssSelector("button[type='submit']");
	By alert = By.cssSelector("div[class='p-3 my-3 text-center bg-danger text-light ng-star-inserted']");



	public LoginPage(WebDriver driver2) {
		this.driver=driver2;
	}

	public WebElement getEmail() {
		return driver.findElement(email);
	}
	public WebElement getPassword() {
		return driver.findElement(password);
	}
	public WebElement getLogin() {
		return driver.findElement(login);
	}
	public WebElement getCode() {
		return driver.findElement(code);
	}
	public WebElement getSubmit() {
		return driver.findElement(submit);
	}
	public WebElement getAlert() {
		return driver.findElement(alert);
	}
}
