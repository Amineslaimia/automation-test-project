package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {
	public WebDriver driver;
	By login = By.cssSelector("a[href='/secure']");
	By cookie = By.cssSelector("a[aria-label='allow cookies']");

	public HomePage(WebDriver driver2) {
		this.driver = driver2;
	}

	public WebElement getLogin() {
		return driver.findElement(login);
	}

	public WebElement getCookies() {
		return driver.findElement(cookie);
	}

}
