package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CreateCoursePage {
    public WebDriver driver;
    By userName = By.cssSelector("input[name='name']");
    By password = By.cssSelector("input[name='pass']");
    By buttonSubmit = By.cssSelector("button[type='submit']");
    By buttonAddCourse = By.cssSelector("a[href='/course/add?destination=/manager/courses']");
    By selectCategory = By.cssSelector("select[name='field_course_category']");
    By selectStatus = By.cssSelector("select[name='field_course_status']");
    By title = By.cssSelector("input[id='edit-title-0-value']");
    By frame = By.xpath("//iframe[@class='cke_wysiwyg_frame cke_reset']");
    By description = By.cssSelector("body.cke_editable.cke_editable_themed.cke_contents_ltr.cke_show_borders:nth-child(2) > p:nth-child(1)");
    By Difficulty = By.cssSelector("input[name='field_difficulty[0][value]']");
    By addGoal = By.cssSelector("button[value='Add new Course  goal']");
    By addGoalInput = By.cssSelector("input[name='field_course_goals[form][0][name][0][value]']");
    By submitNewGoal = By.cssSelector("button[value='Create Course  goal']");
    By startDate = By.cssSelector("input[id='edit-field-course-date-0-value-date']");
    By startTime = By.cssSelector("input[id='edit-field-course-date-0-value-time']");
    By endDate = By.cssSelector("input[name='field_course_date[0][end_value][date]'");
    By endTime = By.cssSelector("input[name='field_course_date[0][end_value][time]']");
    By application = By.cssSelector("a[href='#edit-group-application']");
    By participants = By.cssSelector("input[name='field_max_participants[0][value]']");
    By appStartDate = By.cssSelector("input[name='field_application_date[0][value][date]']");
    By appEndDate = By.cssSelector("input[name='field_application_date[0][end_value][date]']");
    By appStartTime = By.cssSelector("input[name='field_application_date[0][value][time]']");
    By appEndTime = By.cssSelector("input[name='field_application_date[0][end_value][time]']");
    By addReqButton = By.cssSelector("button[value='Add new Requirement']");
    By requirement = By.cssSelector("input[name='field_application_requirements[form][0][name][0][value]']");
    By submitNewReq = By.cssSelector("input[value='Create Requirement']");


    By save = By.cssSelector("button[id='edit-submit']");



    public CreateCoursePage(WebDriver driver2) {
        this.driver = driver2;
    }

    public WebElement getUserName() {
        return driver.findElement(userName);
    }
    public WebElement getPassword() {
        return driver.findElement(password);
    }
    public WebElement getSubmit() {
        return driver.findElement(buttonSubmit);
    }
    public WebElement getAddCourse() {
        return driver.findElement(buttonAddCourse);
    }
    public Select getSelectCategory() {
        Select categorySelect =  new Select(driver.findElement(selectCategory));
        return categorySelect;
    }
    public Select getSelectStatus() {
        Select StatusSelect =  new Select(driver.findElement(selectStatus));
        return StatusSelect;
    }
    public WebElement getTitle() {
        return driver.findElement(title);
    }
    public WebElement getFrame() {
        return driver.findElement(frame);
    }
    public WebElement getDescription() {
        return driver.findElement(description);
    }
    public WebElement getDifficulty() {
        return driver.findElement(Difficulty);
    }
    public WebElement getAddGoal() {
        return driver.findElement(addGoal);
    }
    public WebElement getAddGoalInput() {
        return driver.findElement(addGoalInput);
    }
    public WebElement getSubmitNewGoal() {
        return driver.findElement(submitNewGoal);
    }
    public WebElement getStartDate() {
        return driver.findElement(startDate);
    }
    public WebElement getEndDate() {
        return driver.findElement(endDate);
    }
    public WebElement getStartTime() {
        return driver.findElement(startTime);
    }
    public WebElement getEndTime() {
        return driver.findElement(endTime);
    }
    public WebElement getApplication() {
        return driver.findElement(application);
    }
    public WebElement getParticipants() {
        return driver.findElement(participants);
    }
    public WebElement getAppStartDate() {
        return driver.findElement(appStartDate);
    }
    public WebElement getAppEndDate() {
        return driver.findElement(appEndDate);
    }
    public WebElement getAppStartTime() {
        return driver.findElement(appStartTime);
    }
    public WebElement getAppEndTime() {
        return driver.findElement(appEndTime);
    }
    public WebElement getAddReqButton() {
        return driver.findElement(addReqButton);
    }
    public WebElement getReq() {
        return driver.findElement(requirement);
    }
    public WebElement getSubmitReq() {
        return driver.findElement(submitNewReq);
    }
    public WebElement getSave() {
        return driver.findElement(save);
    }
}

