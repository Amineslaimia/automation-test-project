package Firstproject.exempleOne;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.CreateCoursePage;
import recources.base;
import java.io.IOException;


public class CreateCourse extends base {


@Test
    public void LoginManager() throws IOException {
    String courseTitle="automated Test 1";
        driver= initDriver();
    System.out.println("driver initiated");

        driver.get("http://coa-local.staging.uteek.net/");
        driver.manage().window().maximize();
        CreateCoursePage cc = new CreateCoursePage(driver);
        cc.getUserName().sendKeys("cm manager");
        cc.getPassword().sendKeys("test@cm@2020");
        cc.getSubmit().click();
        driver.get("http://coa-local.staging.uteek.net/course/add?destination=/manager/courses");
cc.getSelectCategory().selectByVisibleText("Development");
cc.getSelectStatus().selectByVisibleText("Registration");
cc.getTitle().sendKeys(courseTitle);
    driver.switchTo().frame(cc.getFrame());
    cc.getDescription().sendKeys("this is an automated test by selenium");
    driver.switchTo().defaultContent();
    cc.getDifficulty().sendKeys("2");
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("window.scrollBy(0,1000)");
cc.getAddGoal().click();
    cc.getAddGoalInput().sendKeys("new goal testing");
    cc.getSubmitNewGoal().click();
       cc.getStartDate().sendKeys("01012020");
       cc.getStartTime().sendKeys("1000am");
       cc.getEndDate().sendKeys("01012021");
       cc.getEndTime().sendKeys("1000am");
       cc.getApplication().click();
       cc.getParticipants().sendKeys("10");
       cc.getAppStartDate().sendKeys("01012020");
       cc.getAppStartTime().sendKeys("1000");
       cc.getAppEndDate().sendKeys("01012021");
       cc.getAppEndTime().sendKeys("1000");
    js.executeScript("window.scrollBy(0,1000)");
       cc.getAddReqButton().click();

       cc.getReq().sendKeys("nothing is required");
       cc.getSave().click();

        Assert.assertEquals( driver.findElement(By.xpath("//tr[1]//td[3]//a[1]")).getText(),courseTitle);



    }


}
