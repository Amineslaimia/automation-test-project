package Firstproject.exempleOne;

import org.testng.Assert;
import pageObjects.LoginPage;
import recources.base;
import com.twilio.Twilio;
import com.twilio.base.ResourceSet;
import com.twilio.rest.api.v2010.account.Message;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;




import java.io.IOException;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;





public class FirstTest extends base {
	public static final String ACCOUNT_SID = "ACcc661ca1174a85c04fee8a4b4b48bd27";
	public static final String AUTH_TOKEN = "2e09b7e0dd2f08ee7c17b2b56ff887ce";
	@Test(dataProvider="getData")
	public void loginInvalidUsers(String email,String password , String text) throws IOException {
		driver = initDriver();
		System.out.print("driver initiated");
		driver.get("https://coa.staging.uteek.net/secure/login");

		LoginPage lp = new LoginPage(driver);

		driver.manage().window().maximize();

		lp.getEmail().sendKeys(email);
		lp.getPassword().sendKeys(password);
		System.out.println(text);
		lp.getLogin().click();
		Assert.assertEquals(lp.getAlert().getText(),"Please verify your inputs");


	}
	@Test
	public void loginValidUser () throws IOException{
		driver = initDriver();
		driver.get("https://coa.staging.uteek.net/secure/login");

		LoginPage lp = new LoginPage(driver);

		driver.manage().window().maximize();

		lp.getEmail().sendKeys("testamineslaimia@yopmail.com");
		lp.getPassword().sendKeys("123456789Aa");

		lp.getLogin().click();

			Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
			String smsBody = getMessage();

			lp.getCode().sendKeys(smsBody.substring(19));
			lp.getSubmit().click();
			Assert.assertEquals(driver.getTitle(),"Candid Academy");




	}





	public static String getMessage () {
		return getMessages().filter(m -> m.getDirection().compareTo(Message.Direction.INBOUND)==0)
				.filter(m -> m.getTo().equals("+19285636098")).map(Message::getBody).findFirst().
				orElseThrow(IllegalStateException::new);	}
	private static Stream<Message> getMessages() {
		ResourceSet<Message> messages = Message.reader(ACCOUNT_SID).read();
		return StreamSupport.stream(messages.spliterator(), false);
		
	}

	@DataProvider
	public Object[][] getData() {
		Object[][] data = new Object[3][3];
		data[0][0]="invaliduser1@email.com";
		data[0][1]="123";
		data[0][2]="invalid user";
		
		data[1][0]="invaliduser2@email.com";
		data[1][1]="123456";
		data[1][2]="invalid user";
		
		data[2][0]="invaliduser3@email.com";
		data[2][1]="invaliduser2@email.com";
		data[2][2]="invalid user";
		return data;

	}
	

}
