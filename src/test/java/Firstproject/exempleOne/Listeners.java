package Firstproject.exempleOne;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.testng.ITestContext;
import recources.ExtendReporterNG;
import org.testng.ITestListener;
import org.testng.ITestResult;
import recources.base;
import recources.lastmodefied;
import Firstproject.exempleOne.SendEmail.*;
import recources.sendemail;


public class Listeners extends base implements ITestListener {
    ExtentTest test;
    ExtentReports extent;

    {
        extent = ExtendReporterNG.getReportObject();
    }

    ThreadLocal<ExtentTest> extentTest =new ThreadLocal<>();
    @Override
    public void onTestStart(ITestResult result) {
    	System.out.println("test started");
        test= extent.createTest(result.getMethod().getMethodName());
        extentTest.set(test);

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        extentTest.get().log(Status.PASS, "Test Passed");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        extentTest.get().log(Status.FAIL, "Test FAILED ");

    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("started");

    }

    @Override
    public void onFinish(ITestContext context) {
        extent.flush();
        sendemail.sendReport();

    }
}
